# Simple Cipher

## Description
Python based decyphering tool for National Cyber League competition.

Ciphers
- Caesar Cipher
- Atbash
- Railfence

Encoding

- base64 to ASCII
- hex to ASCII
- binary to ASCII


## Usage
    # base64 'This is a message'
    python3 test_inc.py "VGhpcyBpcyBhIG1lc3NhZ2U="

## Installation
You should just be able to clone and run as long at pycipher is installed. 
#### Requirements
- Python 3
- [pycipher](https://pypi.org/project/pycipher/)
- others? Raise an issue if you find other dependencies that were missed.

## Project status
I am currently working on this project intermitently as needed to improve my design.  Eventually I am hoping include a full menu system with colored markup.

## Support
No support is being offered at this time.  The project works for me however, YMMV.

## Contributing
No contributions being accepted at this time.  

However, feel free to clone and improve. =)

## License
For open source projects, say how it is licensed.


